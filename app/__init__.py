import logging
import argparse


logger = logging.getLogger('app')

logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(thread)d - %(levelname)s - %(message)s')

fh = logging.FileHandler('download.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(ch)

parser = argparse.ArgumentParser(description='User database utility')
parser.add_argument('-t', '--threads', help='Number of threads', type=int, default=4)
parser.add_argument('-b', '--batch', help='Number of urls in one database query', type=int, default=100)

args = parser.parse_args()
