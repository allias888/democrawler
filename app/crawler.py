import logging
import os
import random
import signal
from threading import Thread

import requests
from requests import RequestException

from .db import Urls, Proxies, Data, db_session, commit


logger = logging.getLogger('app')


@db_session
def download_pages(batch_size):
    """
    General function for download batch of urls.
    :param batch_size (int): count of urls loaded from db and htmls saved to db
    :return (None):
    """
    urls = Urls.pending(batch_size)
    while urls:
        proxies = Proxies.all()
        for url in urls:
            proxy_url = random.choice(proxies).make_url()

            body = fetch_url(url.url, proxy_url)
            if body:
                Data(url=url, html=body)
                url.status = Urls.DOWNLOADED

        commit()

        urls = Urls.pending(batch_size)


def fetch_url(url, proxy_url):
    """
    Fetching url thought proxy server
    :param url (str):
    :param proxy_url (str):
    :return (str): url content or None
    """
    try:
        r = requests.get(url, proxies={'http:': proxy_url}, timeout=300)
        html = r.text
        logger.info('requested: proxy: {}, url: {}'.format(proxy_url, url))
    except RequestException:
        logger.exception('exception: proxy: {}, url: {}'.format(proxy_url, url))
        html = None
    return html


def run(threads, batch_size):
    """
    Entry point, starting downloading urls in few threads
    On exit or interrupt set 'running' status of currently fetching urls back to 'pending'
    :param threads (int): number of threads
    :param batch_size (int): number of urls fetched from db at once
    :return:
    """
    try:
        for _ in range(threads):
            thread = Thread(target=download_pages, args=(batch_size,))
            thread.start()
        signal.pause()
    except (KeyboardInterrupt, SystemExit):
        logger.exception('Keyboard interrupt or system exit')
    finally:
        logger.info('exiting')
        with db_session:
            urls = Urls.select(lambda u: u.status == Urls.RUNNING)
            for url in urls:
                url.status = Urls.PENDING
        os._exit(1)
