from time import sleep
from pony.orm import *


db = Database()
db.bind('postgres', user='demo', password='demo', database='demohtml')


class Urls(db.Entity):
    """
    Table with urls

    status field used for tracking download state
    status:
    -'pending' - default state when urls are in table
    -'running' - trying to fetch url data
    -'downloaded' - url fetched and its content added to Data table
    """
    PENDING = 'p'
    RUNNING = 'r'
    DOWNLOADED = 'd'

    idx = PrimaryKey(int, auto=True)
    url = Required(str, 4000)
    status = Required(str, 1, default=PENDING)
    data = Optional("Data")

    @classmethod
    def pending(cls, limit=100):
        """
        Returning 'limit' urls in pending state and sets its status
        to running in same transaction for thread safety
        :param limit (int): batch size
        """
        urls = select(u for u in cls
                      if u.status == Urls.PENDING).for_update()[:limit]
        for url in urls:
            url.status = Urls.RUNNING
        commit()
        return urls


class Proxies(db.Entity):
    """
    Table with proxies, proxy can only have host and port fields,
    login and password is optional
    """
    host = Required(str, 15)
    port = Required(int, size=16, unsigned=True)
    login = Optional(str, 100)
    password = Optional(str, 100)

    @classmethod
    def all(cls, seconds=5):
        """
        :param seconds (int): time to wait, if no proxies in table
        :return list(proxies objects):
        """
        while True:
            proxies = list(cls.select())
            if proxies:
                return proxies
            sleep(seconds)

    def make_url(self):
        if self.login and self.password:
            url = 'http://{}:{}@{}:{}'.format(self.login, self.password,
                                              self.host, self.port)
        else:
            url = 'http://{}:{}'.format(self.host, self.port)
        return url


class Data(db.Entity):
    """
    Table for fetched urls content
    """
    url = Required("Urls")
    html = Required(str)


db.generate_mapping(create_tables=True)
