#!/usr/bin/env bash

export DB="demohtml"
export PGUSER="demo"
export PGPASSWORD="demo"

export TEST_DB="test_demohtml"

# count is number of rows inserted in users table
count=100000

function create() {
sudo -u postgres psql -c "CREATE DATABASE $DB;"
sudo -u postgres psql -d $DB -c "CREATE USER $PGUSER WITH PASSWORD '$PGPASSWORD';
                                 GRANT ALL privileges ON DATABASE $DB TO $PGUSER;"

source_dir="$( cd "$( dirname "$0")"/ ; pwd -P )"
$source_dir/venv/bin/python $source_dir/app/db.py
}

function insert() {
    sudo -u postgres PGPASSWORD=$PGPASSWORD psql -d $DB -U $PGUSER -c "
               INSERT INTO urls (url, status)
               SELECT 'http://google.com/' as url, 'p' AS status
               from generate_series(1,$count);"
    sudo -u postgres PGPASSWORD=$PGPASSWORD psql -d $DB -U $PGUSER -c "
               INSERT INTO proxies (host, port, login, password) values
               ('104.28.0.5', 80, '', ''), ('36.67.28.178', 8080, '', '');"
}


function flush_running() {
    sudo -u postgres PGPASSWORD=$PGPASSWORD psql -d $DB -U $PGUSER -c "
               UPDATE urls SET status = 'p' WHERE status = 'd';"
}


if [ "$2" -eq "$2" ] 2>/dev/null
then
    count=$2
fi


if [ $1 = "create" ]
then
    echo "creating tables"
    create
fi

if [ $1 = "insert" ]
then
    echo "inserting $count rows into urls"
    insert
fi


if [ $1 = "flush" ]
then
    echo "reseting running state of all urls"
    flush_running
fi

