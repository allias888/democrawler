import argparse

from app.crawler import run


parser = argparse.ArgumentParser(description='User database utility')
parser.add_argument('-t', '--threads', help='Number of threads', type=int, default=4)
parser.add_argument('-b', '--batch', help='Number of urls in one database query', type=int, default=100)

args = parser.parse_args()


run(args.threads, args.batch)
